# :first_in sets how long it takes before the job is first run. In this case, it is run immediately
SCHEDULER.every '1m', :first_in => 0 do |job|

  conn = Faraday.new(:url => 'http://matchthecity.herokuapp.com') do |faraday|
  faraday.request  :url_encoded             # form-encode POST params
  faraday.response :logger                  # log requests to STDOUT
  faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
end

## GET ##

response = conn.get '/venues.json'     # GET hhttp://matchthecity.herokuapp.com/venues.json
venues = response.body

response = conn.get '/opportunities.json'     # GET hhttp://matchthecity.herokuapp.com/opportunities.json
opportunities = response.body


JSON
  venues_data = JSON.parse(venues)
  opportunities_data = JSON.parse(opportunities)

  groups = opportunities_data.group_by{|o| o["venue_id"] }
  result = []
  groups.each do |k,v|
    result << {label: venue_with_id(k, venues_data)["name"], value: v.count}
  end
  result.sort!{|a,b| a[:label] <=> b[:label]}

  send_event('sports_venues', { items: result })
end

def venue_with_id(venue_id, venues_data)

  venues_data.each do |v|
    return v if v["id"] == venue_id
  end
end