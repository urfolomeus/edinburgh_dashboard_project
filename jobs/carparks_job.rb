require 'faraday'

SCHEDULER.every '5m' do

conn = Faraday.new(:url => 'http://open311.xoverto.com') do |faraday|
  faraday.request  :url_encoded             # form-encode POST params
  faraday.response :logger                  # log requests to STDOUT
  faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
end

## GET ##

response = conn.get '/dev/v1/facilities/parking.json'     # GET http://open311.xoverto.com/dev/v1/facilities/parking.json
carparks = response.body

JSON
  carparks_data = JSON.parse(carparks)
  result = carparks_data
    .map{|cp| {label: cp["facility_name"], value: free_spaces(cp)}}
    .sort{|a,b| a[:label] <=> b[:label]}
  send_event('carparks', { items: result })
end

def free_spaces(carpark)
  occupancy = carpark["features"]["occupancy"].to_i
  occupancy_percentage = carpark["features"]["occupancy_percentage"].to_f

  if occupancy_percentage == 0
    spaces = 'closed'
  else
    capacity = (occupancy / occupancy_percentage) * 100
    spaces = (capacity / 100) *  (100 - occupancy_percentage)
    spaces = spaces.to_i
  end
  spaces
end
