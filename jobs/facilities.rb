# :first_in sets how long it takes before the job is first run. In this case, it is run immediately
SCHEDULER.every '1m', :first_in => 0 do |job|

  conn = Faraday.new(:url => 'http://open311.xoverto.com') do |faraday|
    faraday.request  :url_encoded             # form-encode POST params
    faraday.response :logger                  # log requests to STDOUT
    faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
  end

  ## GET ##

  response = conn.get '/dev/v1/facilities/all.json'     # GET http://open311.xoverto.com/dev/v1/facilities/all.json
  schools = response.body


  JSON
    schools_data = JSON.parse(schools)
    groups = schools_data.group_by{|f| f["type"] }
    result = []
    groups.each do |k,v|
      result << {label: k, value: v.count}
    end
    result.sort!{|a,b| a[:label] <=> b[:label]}

    send_event('facilities', { items: result })
  end
